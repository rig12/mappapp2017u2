﻿using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;

[assembly: AssemblyCompany("Schneider Electric Software, LLC")]
[assembly: AssemblyCopyright("© 2018 Schneider Electric Software, LLC. All rights reserved.")]
[assembly: AssemblyTrademark("Schneider Electric, Wonderware and ArchestrA are trademarks of Schneider Electric SE, its subsidiaries and affiliated companies.")]
[assembly: AssemblyProduct("Wonderware System Platform")]
[assembly: AssemblyInformationalVersion("17.2.000")]
[assembly: AssemblyFileVersion("2717.0426.1163.1")]
[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "ArchestrA.Visualization.MapApp")]
[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "ArchestrA.Visualization.MapApp.JavaScriptApi")]
[assembly: SuppressMessage("Microsoft.Design", "CA1014:MarkAssembliesWithClsCompliant")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1824:MarkAssembliesWithNeutralResourcesLanguage")]
[assembly: AssemblyTitle("ArchestrA.Visualization.MapApp")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]
[assembly: AssemblyVersion("1.0.0.0")]
