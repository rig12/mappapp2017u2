﻿// Decompiled with JetBrains decompiler
// Type: ArchestrA.Visualization.MapApp.JavaScriptApi.JSProperties
// Assembly: ArchestrA.Visualization.MapApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=23106a86e706d0ae
// MVID: 7B8B8207-B824-4FA0-A1DC-FED604EEF8DD
// Assembly location: D:\Support\Руминтек\20191209 MapApp Scale-Tyle &  ZoomValue\2017U2\MapApp\697\ArchestrA.Visualization.MapApp.dll

using System.Collections.Generic;
using System.ComponentModel;

namespace ArchestrA.Visualization.MapApp.JavaScriptApi
{
  public class JSProperties : INotifyPropertyChanged
  {
    private Dictionary<string, object> properties = new Dictionary<string, object>();

    public event PropertyChangedEventHandler PropertyChanged;

    public object GetProperty(string name)
    {
      object obj;
      this.properties.TryGetValue(name, out obj);
      return obj;
    }

    public void SetProperty(string name, object value)
    {
      if (this.properties.ContainsKey(name))
        this.properties.Remove(name);
      this.properties.Add(name, value);
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(name));
    }
  }
}
