﻿// Decompiled with JetBrains decompiler
// Type: ArchestrA.Visualization.MapApp.JavaScriptApi.JSViewApp
// Assembly: ArchestrA.Visualization.MapApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=23106a86e706d0ae
// MVID: 7B8B8207-B824-4FA0-A1DC-FED604EEF8DD
// Assembly location: D:\Support\Руминтек\20191209 MapApp Scale-Tyle &  ZoomValue\2017U2\MapApp\697\ArchestrA.Visualization.MapApp.dll

using ArchestrA.Client.WebControls;
using ArchestrA.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ArchestrA.Visualization.MapApp.JavaScriptApi
{
  public class JSViewApp : IDisposable
  {
    private JSNavigation navigation;
    private JSProperties properties;
    private bool disposed;

    public JSViewApp(WebControl browser)
    {
      this.navigation = new JSNavigation(browser);
      this.properties = new JSProperties();
      browser.RegisterJSFunction("viewAppGetProperty", new JSFunctionInvokeHandler(this.GetProperty));
      browser.RegisterJSFunction("viewAppGetCurrentAsset", new JSFunctionInvokeHandler(this.GetCurrentAsset));
      browser.RegisterJSFunction("viewAppSetCurrentAsset", new JSFunctionInvokeHandler(this.SetCurrentAsset));
    }

    ~JSViewApp()
    {
      try
      {
        this.Dispose(false);
        Logger.LogDispose(this.GetType().FullName);
      }
      catch
      {
      }
      finally
      {
        // ISSUE: explicit finalizer call
        //base.Finalize();
      }
    }

    private void GetProperty(object sender, JSFunctionInvokeEventArgs e)
    {
      IReadOnlyCollection<object> arguments = e.Arguments;
      if (arguments == null || arguments.Count != 1)
        return;
      e.ReturnValue = this.properties.GetProperty(arguments.First<object>() as string);
    }

    private void GetCurrentAsset(object sender, JSFunctionInvokeEventArgs e)
    {
      e.ReturnValue = (object) this.navigation.CurrentAsset;
    }

    private void SetCurrentAsset(object sender, JSFunctionInvokeEventArgs e)
    {
      IReadOnlyCollection<object> arguments = e.Arguments;
      if (arguments == null || arguments.Count != 1)
        return;
      this.navigation.CurrentAsset = arguments.First<object>() as string;
    }

    public JSNavigation Navigation
    {
      get
      {
        return this.navigation;
      }
    }

    public JSProperties Properties
    {
      get
      {
        return this.properties;
      }
    }

    public void Dispose()
    {
      this.Dispose(true);
      GC.SuppressFinalize((object) this);
    }

    protected virtual void Dispose(bool disposing)
    {
      if (!disposing || this.disposed)
        return;
      this.navigation?.Dispose();
      this.navigation = (JSNavigation) null;
      this.disposed = true;
    }
  }
}
