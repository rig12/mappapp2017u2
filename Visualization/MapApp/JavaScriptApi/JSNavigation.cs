﻿// Decompiled with JetBrains decompiler
// Type: ArchestrA.Visualization.MapApp.JavaScriptApi.JSNavigation
// Assembly: ArchestrA.Visualization.MapApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=23106a86e706d0ae
// MVID: 7B8B8207-B824-4FA0-A1DC-FED604EEF8DD
// Assembly location: D:\Support\Руминтек\20191209 MapApp Scale-Tyle &  ZoomValue\2017U2\MapApp\697\ArchestrA.Visualization.MapApp.dll

using ArchestrA.Client.MyViewApp;
using ArchestrA.Client.WebControls;
using ArchestrA.Diagnostics;
using System;
using System.ComponentModel;
using System.Globalization;

namespace ArchestrA.Visualization.MapApp.JavaScriptApi
{
    public class JSNavigation : INotifyPropertyChanged, IDisposable
    {
        private string currentAsset = "US";
        private WebControl wpfBrowser;
        private string currentArea;
        private bool disposed;

        public event PropertyChangedEventHandler PropertyChanged;

        public JSNavigation(WebControl browser)
        {
            this.wpfBrowser = browser;
            try
            {
                Navigation.PropertyChanged += new PropertyChangedEventHandler(this.Navigation_PropertyChanged);
            }
            catch
            {
            }
        }

        ~JSNavigation()
        {
            try
            {
                this.Dispose(false);
                Logger.LogDispose(this.GetType().FullName);
            }
            catch
            {
            }
            finally
            {
                // ISSUE: explicit finalizer call
                //base.Finalize();
            }
        }

        public string CurrentAsset
        {
            get
            {
                try
                {
                    return Navigation.CurrentAsset;
                }
                catch
                {
                    return this.currentAsset;
                }
            }
            set
            {
                try
                {
                    this.wpfBrowser.Dispatcher.BeginInvoke((Action)(() => Navigation.CurrentAsset = value));
                }
                catch
                {
                    this.currentAsset = value;
                    this.Navigation_PropertyChanged((object)this, new PropertyChangedEventArgs(nameof(CurrentAsset)));
                }
                PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
                if (propertyChanged == null)
                    return;
                propertyChanged((object)this, new PropertyChangedEventArgs(nameof(CurrentAsset)));
            }
        }

        public string CurrentArea
        {
            get
            {
                try
                {
                    return Navigation.CurrentArea;
                }
                catch
                {
                    return this.currentArea;
                }
            }
            set
            {
                try
                {
                    this.wpfBrowser.Dispatcher.BeginInvoke((Action)(() => { Navigation.CurrentArea = value; }));
                }
                catch
                {
                    this.currentArea = value;
                    this.Navigation_PropertyChanged((object)this, new PropertyChangedEventArgs(nameof(CurrentArea)));
                }
                PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
                if (propertyChanged == null)
                    return;
                propertyChanged((object)this, new PropertyChangedEventArgs(nameof(CurrentArea)));
            }
        }

        public void Navigation_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            string format = "if (typeof viewAppNavigationChanged != \"undefined\") viewAppNavigationChanged('{0}', '{1}')";
            if (e.PropertyName == "CurrentAsset")
            {
                this.wpfBrowser.ExecuteScriptAsync(string.Format((IFormatProvider)CultureInfo.CurrentCulture, format, (object)"currentAsset", (object)this.CurrentAsset));
            }
            else
            {
                if (!(e.PropertyName == "CurrentArea"))
                    return;
                this.wpfBrowser.ExecuteScriptAsync(string.Format((IFormatProvider)CultureInfo.CurrentCulture, format, (object)"currentArea", (object)this.CurrentArea));
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing || this.disposed)
                return;
            Navigation.PropertyChanged -= new PropertyChangedEventHandler(this.Navigation_PropertyChanged);
            this.disposed = true;
        }
    }
}
