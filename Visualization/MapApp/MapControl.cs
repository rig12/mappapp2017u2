﻿// Decompiled with JetBrains decompiler
// Type: ArchestrA.Visualization.MapApp.MapControl
// Assembly: ArchestrA.Visualization.MapApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=23106a86e706d0ae
// MVID: 7B8B8207-B824-4FA0-A1DC-FED604EEF8DD
// Assembly location: D:\Support\Руминтек\20191209 MapApp Scale-Tyle &  ZoomValue\2017U2\MapApp\697\ArchestrA.Visualization.MapApp.dll

using ArchestrA.Client.CommonUtil;
using ArchestrA.Client.WebControls;
using ArchestrA.Visualization.MapApp.JavaScriptApi;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Input;

namespace ArchestrA.Visualization.MapApp
{
    public class MapControl : WebControl, INotifyPropertyChanged, IDisposable
    {
        private double zoom = 5.0;
        private double maxZoom = 80.0;
        private bool selectedAsset = true;
        private string sources = "(All)";
        private string layers = "(All)";
        private double latitude;
        private double longitude;
        private double minZoom;
        private double? maxBoundsSouth;
        private double? maxBoundsWest;
        private double? maxBoundsNorth;
        private double? maxBoundsEast;
        private bool disposed;

        public event PropertyChangedEventHandler PropertyChanged;

        public event EventHandler OnPropertyChange;

        public MapControl()
        {
            string str = "http://localhost:55601/Map/map-runtime.html";
            System.Configuration.Configuration configuration = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);
            if (configuration != null)
            {
                if (configuration.AppSettings.Settings["Url"] != null)
                    str = configuration.AppSettings.Settings["Url"].Value;
                bool result;
                if (configuration.AppSettings.Settings["EnableDevTool"] != null && bool.TryParse(configuration.AppSettings.Settings["EnableDevTool"].Value, out result) && result)
                    this.EnableDevTool();
            }
            this.Url = str;
        }

        protected void NotifyPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged == null)
                return;
            propertyChanged((object)this, new PropertyChangedEventArgs(name));
        }

        [Category("Position")]
        [LocalizedDescription(typeof(MapControl), "ArchestrA.Visualization.MapApp.Properties.Resources", "Latitude_Description")]
        [DataMember]
        public double InitialLatitude
        {
            get
            {
                return this.latitude;
            }
            set
            {
                this.latitude = value;
                this.ViewAppApi?.Properties.SetProperty("Latitude", (object)value);
                this.NotifyPropertyChanged("Latitude");
            }
        }

        [Category("Position")]
        [LocalizedDescription(typeof(MapControl), "ArchestrA.Visualization.MapApp.Properties.Resources", "Longitude_Description")]
        [DataMember]
        public double InitialLongitude
        {
            get
            {
                return this.longitude;
            }
            set
            {
                this.longitude = value;
                this.ViewAppApi?.Properties.SetProperty("Longitude", (object)value);
                this.NotifyPropertyChanged("Longitude");
            }
        }

        [Category("Position")]
        [LocalizedDescription(typeof(MapControl), "ArchestrA.Visualization.MapApp.Properties.Resources", "Zoom_Description")]
        [DataMember]
        public double InitialZoom
        {
            get
            {
                return this.zoom;
            }
            set
            {
                this.zoom = value;
                this.ViewAppApi?.Properties.SetProperty("Zoom", (object)value);
                this.NotifyPropertyChanged("Zoom");
            }
        }

        [Category("Limits")]
        [LocalizedDescription(typeof(MapControl), "ArchestrA.Visualization.MapApp.Properties.Resources", "MinZoom_Description")]
        [DataMember]
        public double MinZoom
        {
            get
            {
                return this.minZoom;
            }
            set
            {
                this.minZoom = value;
                this.ViewAppApi?.Properties.SetProperty(nameof(MinZoom), (object)value);
                this.NotifyPropertyChanged(nameof(MinZoom));
            }
        }

        [Category("Limits")]
        [LocalizedDescription(typeof(MapControl), "ArchestrA.Visualization.MapApp.Properties.Resources", "MaxZoom_Description")]
        [DataMember]
        public double MaxZoom
        {
            get
            {
                return this.maxZoom;
            }
            set
            {
                this.maxZoom = value;
                this.ViewAppApi?.Properties.SetProperty(nameof(MaxZoom), (object)value);
                this.NotifyPropertyChanged(nameof(MaxZoom));
            }
        }

        [Category("Limits")]
        [LocalizedDescription(typeof(MapControl), "ArchestrA.Visualization.MapApp.Properties.Resources", "MaxBoundsSouth_Description")]
        [DataMember]
        public double? MaxBoundsSouth
        {
            get
            {
                return this.maxBoundsSouth;
            }
            set
            {
                this.maxBoundsSouth = value;
                this.ViewAppApi?.Properties.SetProperty(nameof(MaxBoundsSouth), (object)value);
                this.NotifyPropertyChanged(nameof(MaxBoundsSouth));
            }
        }

        [Category("Limits")]
        [LocalizedDescription(typeof(MapControl), "ArchestrA.Visualization.MapApp.Properties.Resources", "MaxBoundsWest_Description")]
        [DataMember]
        public double? MaxBoundsWest
        {
            get
            {
                return this.maxBoundsWest;
            }
            set
            {
                this.maxBoundsWest = value;
                this.ViewAppApi?.Properties.SetProperty(nameof(MaxBoundsWest), (object)value);
                this.NotifyPropertyChanged(nameof(MaxBoundsWest));
            }
        }

        [Category("Limits")]
        [LocalizedDescription(typeof(MapControl), "ArchestrA.Visualization.MapApp.Properties.Resources", "MaxBoundsNorth_Description")]
        [DataMember]
        public double? MaxBoundsNorth
        {
            get
            {
                return this.maxBoundsNorth;
            }
            set
            {
                this.maxBoundsNorth = value;
                this.ViewAppApi?.Properties.SetProperty(nameof(MaxBoundsNorth), (object)value);
                this.NotifyPropertyChanged(nameof(MaxBoundsNorth));
            }
        }

        [Category("Limits")]
        [LocalizedDescription(typeof(MapControl), "ArchestrA.Visualization.MapApp.Properties.Resources", "MaxBoundsEast_Description")]
        [DataMember]
        public double? MaxBoundsEast
        {
            get
            {
                return this.maxBoundsEast;
            }
            set
            {
                this.maxBoundsEast = value;
                this.ViewAppApi?.Properties.SetProperty(nameof(MaxBoundsEast), (object)value);
                this.NotifyPropertyChanged(nameof(MaxBoundsEast));
            }
        }

        [Category("Position")]
        [LocalizedDescription(typeof(MapControl), "ArchestrA.Visualization.MapApp.Properties.Resources", "SelectedAsset_Description")]
        [DataMember]
        public bool LinkCurrentAsset
        {
            get
            {
                return this.selectedAsset;
            }
            set
            {
                this.selectedAsset = value;
                this.ViewAppApi?.Properties.SetProperty("SelectedAsset", (object)value);
                this.NotifyPropertyChanged("SelectedAsset");
            }
        }

        [Category("Content")]
        [LocalizedDescription(typeof(MapControl), "ArchestrA.Visualization.MapApp.Properties.Resources", "Sources_Description")]
        [DataMember]
        public string Sources
        {
            get
            {
                return this.sources;
            }
            set
            {
                this.sources = value;
                this.ViewAppApi?.Properties.SetProperty(nameof(Sources), (object)value);
                this.NotifyPropertyChanged(nameof(Sources));
            }
        }

        [Category("Content")]
        [LocalizedDescription(typeof(MapControl), "ArchestrA.Visualization.MapApp.Properties.Resources", "Layers_Description")]
        [DataMember]
        public string ZoomLayers
        {
            get
            {
                return this.layers;
            }
            set
            {
                this.layers = value;
                this.ViewAppApi?.Properties.SetProperty("Layers", (object)value);
                this.NotifyPropertyChanged("Layers");
            }
        }

        private void InitializeProperty()
        {
            this.ViewAppApi.Properties.SetProperty("Latitude", (object)this.InitialLatitude);
            this.ViewAppApi.Properties.SetProperty("Longitude", (object)this.InitialLongitude);
            this.ViewAppApi.Properties.SetProperty("Zoom", (object)this.InitialZoom);
            this.ViewAppApi.Properties.SetProperty("MinZoom", (object)this.MinZoom);
            this.ViewAppApi.Properties.SetProperty("MaxZoom", (object)this.MaxZoom);
            this.ViewAppApi.Properties.SetProperty("MaxBoundsSouth", (object)this.MaxBoundsSouth);
            this.ViewAppApi.Properties.SetProperty("MaxBoundsWest", (object)this.MaxBoundsWest);
            this.ViewAppApi.Properties.SetProperty("MaxBoundsNorth", (object)this.MaxBoundsNorth);
            this.ViewAppApi.Properties.SetProperty("MaxBoundsEast", (object)this.MaxBoundsEast);
            this.ViewAppApi.Properties.SetProperty("SelectedAsset", (object)this.LinkCurrentAsset);
            this.ViewAppApi.Properties.SetProperty("Sources", (object)this.Sources);
            this.ViewAppApi.Properties.SetProperty("Layers", (object)this.ZoomLayers);
            this.ViewAppApi.Properties.PropertyChanged += new PropertyChangedEventHandler(this.Properties_PropertyChanged);
            this.ViewAppApi.Navigation.PropertyChanged += new PropertyChangedEventHandler(this.Navigation_PropertyChanged);
        }

        private void Navigation_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.NotifyPropertyChanged("CurrentAsset");
        }

        private void Properties_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            EventHandler onPropertyChange = this.OnPropertyChange;
            if (onPropertyChange == null)
                return;
            onPropertyChange(sender, (EventArgs)e);
        }

        public string CurrentAsset
        {
            get
            {
                return this.ViewAppApi?.Navigation.CurrentAsset ?? string.Empty;
            }
            set
            {
                if (this.ViewAppApi != null)
                    this.ViewAppApi.Navigation.CurrentAsset = value;
                this.NotifyPropertyChanged(nameof(CurrentAsset));
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !this.disposed)
                this.ViewAppApi?.Dispose();
            base.Dispose(true);
            this.disposed = true;
        }

        public JSViewApp ViewAppApi { get; private set; }

        protected override void OnInitializeBrowser()
        {
            base.OnInitializeBrowser();
            this.ViewAppApi = new JSViewApp((WebControl)this);
            this.InitializeProperty();
        }

        protected override void OnCertificationError(object sender, CertificateErrorEventArgs e)
        {
            e.Continue();
        }

        public void ExecuteJavaScriptAsync(string scriptContent)
        {
            this.ExecuteScriptAsync(scriptContent);
        }

        public void EnableDevTool()
        {
            this.KeyDown += (KeyEventHandler)((sender, e) =>
           {
               if (e.Key != Key.F12)
                   return;
               this.ShowDevTools();
           });
        }
    }
}