﻿// Decompiled with JetBrains decompiler
// Type: ArchestrA.Visualization.MapApp.Properties.Resources
// Assembly: ArchestrA.Visualization.MapApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=23106a86e706d0ae
// MVID: 7B8B8207-B824-4FA0-A1DC-FED604EEF8DD
// Assembly location: D:\Support\Руминтек\20191209 MapApp Scale-Tyle &  ZoomValue\2017U2\MapApp\697\ArchestrA.Visualization.MapApp.dll

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace ArchestrA.Visualization.MapApp.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (ArchestrA.Visualization.MapApp.Properties.Resources.resourceMan == null)
          ArchestrA.Visualization.MapApp.Properties.Resources.resourceMan = new ResourceManager("ArchestrA.Visualization.MapApp.Properties.Resources", typeof (ArchestrA.Visualization.MapApp.Properties.Resources).Assembly);
        return ArchestrA.Visualization.MapApp.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture;
      }
      set
      {
        ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture = value;
      }
    }

    internal static string Latitude_Description
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.ResourceManager.GetString(nameof (Latitude_Description), ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture);
      }
    }

    internal static string Layers_Description
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.ResourceManager.GetString(nameof (Layers_Description), ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture);
      }
    }

    internal static string Longitude_Description
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.ResourceManager.GetString(nameof (Longitude_Description), ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture);
      }
    }

    internal static string MaxBoundsEast_Description
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.ResourceManager.GetString(nameof (MaxBoundsEast_Description), ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture);
      }
    }

    internal static string MaxBoundsNorth_Description
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.ResourceManager.GetString(nameof (MaxBoundsNorth_Description), ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture);
      }
    }

    internal static string MaxBoundsSouth_Description
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.ResourceManager.GetString(nameof (MaxBoundsSouth_Description), ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture);
      }
    }

    internal static string MaxBoundsWest_Description
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.ResourceManager.GetString(nameof (MaxBoundsWest_Description), ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture);
      }
    }

    internal static string MaxZoom_Description
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.ResourceManager.GetString(nameof (MaxZoom_Description), ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture);
      }
    }

    internal static string MinZoom_Description
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.ResourceManager.GetString(nameof (MinZoom_Description), ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture);
      }
    }

    internal static string SelectedAsset_Description
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.ResourceManager.GetString(nameof (SelectedAsset_Description), ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture);
      }
    }

    internal static string Sources_Description
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.ResourceManager.GetString(nameof (Sources_Description), ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture);
      }
    }

    internal static string Zoom_Description
    {
      get
      {
        return ArchestrA.Visualization.MapApp.Properties.Resources.ResourceManager.GetString(nameof (Zoom_Description), ArchestrA.Visualization.MapApp.Properties.Resources.resourceCulture);
      }
    }
  }
}
