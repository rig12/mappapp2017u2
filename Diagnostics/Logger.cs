﻿// Decompiled with JetBrains decompiler
// Type: ArchestrA.Diagnostics.Logger
// Assembly: ArchestrA.Visualization.MapApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=23106a86e706d0ae
// MVID: 7B8B8207-B824-4FA0-A1DC-FED604EEF8DD
// Assembly location: D:\Support\Руминтек\20191209 MapApp Scale-Tyle &  ZoomValue\2017U2\MapApp\697\ArchestrA.Visualization.MapApp.dll

using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

namespace ArchestrA.Diagnostics
{
  [ExcludeFromCodeCoverage]
  internal static class Logger
  {
    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    public static void LogError(Func<string> getMessage, Exception exception = null)
    {
      if (Logger.ArchestrALogger.IsInstalled)
        Logger.ArchestrALogger.LogError(getMessage, exception);
      else
        Logger.WindowsLogger.WriteEntry(getMessage(), EventLogEntryType.Error);
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    public static void LogWarning(Func<string> getMessage, Exception exception = null)
    {
      if (Logger.ArchestrALogger.IsInstalled)
        Logger.ArchestrALogger.LogWarning(getMessage, exception);
      else
        Logger.WindowsLogger.WriteEntry(getMessage(), EventLogEntryType.Warning);
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    public static void LogInfo(Func<string> getMessage)
    {
      if (Logger.ArchestrALogger.IsInstalled)
        Logger.ArchestrALogger.LogInfo(getMessage);
      else
        Logger.WindowsLogger.WriteEntry(getMessage(), EventLogEntryType.Information);
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    public static void LogTrace(Func<string> getMessage, Exception exception = null)
    {
      if (Logger.ArchestrALogger.IsInstalled)
        Logger.ArchestrALogger.LogTrace(getMessage, exception);
      else
        Logger.WindowsLogger.WriteEntry(getMessage(), EventLogEntryType.Information);
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    public static void LogCustom(IntPtr customLogHandle, Func<string> getMessage)
    {
      if (Logger.ArchestrALogger.IsInstalled)
        Logger.ArchestrALogger.LogCustom(customLogHandle, getMessage);
      else
        Logger.WindowsLogger.WriteEntry(getMessage(), EventLogEntryType.Information);
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    public static void LogEntry(Func<string> getMessage, [CallerMemberName] string callerName = "")
    {
      if (Logger.ArchestrALogger.IsInstalled)
        Logger.ArchestrALogger.LogEntryExit(getMessage, "Entered", callerName);
      else
        Logger.WindowsLogger.WriteEntry(getMessage(), EventLogEntryType.Information);
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    public static void LogExit(Func<string> getMessage, [CallerMemberName] string callerName = "")
    {
      if (Logger.ArchestrALogger.IsInstalled)
        Logger.ArchestrALogger.LogEntryExit(getMessage, "Exiting", callerName);
      else
        Logger.WindowsLogger.WriteEntry(getMessage(), EventLogEntryType.Information);
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    public static void LogSQL(Func<string> getMessage)
    {
      if (Logger.ArchestrALogger.IsInstalled)
        Logger.ArchestrALogger.LogSQL(getMessage);
      else
        Logger.WindowsLogger.WriteEntry(getMessage(), EventLogEntryType.Information);
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    public static void LogConnection(Func<string> getMessage)
    {
      if (Logger.ArchestrALogger.IsInstalled)
        Logger.ArchestrALogger.LogConnection(getMessage);
      else
        Logger.WindowsLogger.WriteEntry(getMessage(), EventLogEntryType.Information);
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    public static void LogDispose(string className)
    {
      Logger.LogWarning((Func<string>) (() => string.Format((IFormatProvider) CultureInfo.InvariantCulture, "Failed to call Dispose() for class {0}. Clean up of unmanaged resource was deferred to garbage collector.", (object) className)), (Exception) null);
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    public static IntPtr RegisterCustomLogFlag(string customLogFlagName)
    {
      if (string.IsNullOrEmpty(customLogFlagName))
        return IntPtr.Zero;
      return Logger.ArchestrALogger.RegisterCustomLogFlag(customLogFlagName);
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    public static void SetIndentityName(string identityName)
    {
      if (string.IsNullOrEmpty(identityName))
        return;
      Logger.ArchestrALogger.LogSetIdentityName(identityName);
    }

    [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
    private static string FormatMessageWithCallerInfo(string message)
    {
      string str = string.Format((IFormatProvider) CultureInfo.InvariantCulture, " \nCall Stack: \n{0}", (object) new StackTrace(3, true));
      if (!string.IsNullOrEmpty(message))
        return message + str;
      return str;
    }

    private static class ArchestrALogger
    {
      [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Avoid property for performance. This feild is visible only to the parent class")]
      internal static bool IsInstalled = Logger.ArchestrALogger.IsLoggerInstalled();
      private static int identity = 0;
      private static bool checkRegistry = true;
      private static IntPtr[] logFlags = new IntPtr[11];
      private static object lockObj = new object();
      private static IntPtr includeCallerInfoLogFlag;
      private static bool isLoaded;
      private static bool domainUnloaded;

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static int ErrorCount
      {
        get
        {
          if (!Logger.ArchestrALogger.InitLoggerDll())
            return -1;
          int errorCount = 0;
          int warningCount = 0;
          long ftLastError = 0;
          long ftLastWarning = 0;
          if (Logger.NativeMethods.GetLoggerStats(string.Empty, ref errorCount, ref ftLastError, ref warningCount, ref ftLastWarning) <= 0)
            return -1;
          return errorCount;
        }
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static int WarningCount
      {
        get
        {
          if (!Logger.ArchestrALogger.InitLoggerDll())
            return -1;
          int errorCount = 0;
          int warningCount = 0;
          long ftLastError = 0;
          long ftLastWarning = 0;
          if (Logger.NativeMethods.GetLoggerStats(string.Empty, ref errorCount, ref ftLastError, ref warningCount, ref ftLastWarning) <= 0)
            return -1;
          return warningCount;
        }
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static void LogError(Func<string> getMessage, Exception exception)
      {
        if (Logger.ArchestrALogger.identity == 0 && !Logger.ArchestrALogger.Initialize() || !Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.logFlags[0], 0))
          return;
        string message = Logger.ArchestrALogger.FormatMessageWithException(getMessage, exception);
        Logger.NativeMethods.LogError(Logger.ArchestrALogger.identity, Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.includeCallerInfoLogFlag, 0) ? Logger.FormatMessageWithCallerInfo(message) : message);
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static void LogWarning(Func<string> getMessage, Exception exception)
      {
        if (Logger.ArchestrALogger.identity == 0 && !Logger.ArchestrALogger.Initialize() || !Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.logFlags[1], 0))
          return;
        string message = Logger.ArchestrALogger.FormatMessageWithException(getMessage, exception);
        Logger.NativeMethods.LogWarning(Logger.ArchestrALogger.identity, Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.includeCallerInfoLogFlag, 0) ? Logger.FormatMessageWithCallerInfo(message) : message);
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static void LogInfo(Func<string> getMessage)
      {
        if (Logger.ArchestrALogger.identity == 0 && !Logger.ArchestrALogger.Initialize() || (getMessage == null || !Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.logFlags[2], 0)))
          return;
        Logger.NativeMethods.LogInfo(Logger.ArchestrALogger.identity, Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.includeCallerInfoLogFlag, 0) ? Logger.FormatMessageWithCallerInfo(getMessage()) : getMessage());
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static void LogTrace(Func<string> getMessage, Exception exception)
      {
        if (Logger.ArchestrALogger.identity == 0 && !Logger.ArchestrALogger.Initialize() || !Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.logFlags[3], 0))
          return;
        string message = Logger.ArchestrALogger.FormatMessageWithException(getMessage, exception);
        Logger.NativeMethods.LogTrace(Logger.ArchestrALogger.identity, Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.includeCallerInfoLogFlag, 0) ? Logger.FormatMessageWithCallerInfo(message) : message);
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static void LogConnection(Func<string> getMessage)
      {
        if (Logger.ArchestrALogger.identity == 0 && !Logger.ArchestrALogger.Initialize() || (getMessage == null || !Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.logFlags[8], 0)))
          return;
        Logger.NativeMethods.LogConnection(Logger.ArchestrALogger.identity, Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.includeCallerInfoLogFlag, 0) ? Logger.FormatMessageWithCallerInfo(getMessage()) : getMessage());
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static void LogCustom(IntPtr customFlag, Func<string> getMessage)
      {
        if (Logger.ArchestrALogger.identity == 0 && !Logger.ArchestrALogger.Initialize() || (getMessage == null || !Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, customFlag, 0)))
          return;
        Logger.NativeMethods.LogCustom(Logger.ArchestrALogger.identity, customFlag, Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.includeCallerInfoLogFlag, 0) ? Logger.FormatMessageWithCallerInfo(getMessage()) : getMessage());
      }

      [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "Log messages use invariant culture.", MessageId = "ArchestrA.Diagnostics.Logger+NativeMethods.LogEntryExit(System.Int32,System.String)")]
      [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "Log messages use invariant culture.", MessageId = "ArchestrA.Diagnostics.Logger.FormatMessageWithCallerInfo(System.String)")]
      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static void LogEntryExit(Func<string> getMessage, string prefix, string callerName)
      {
        if (Logger.ArchestrALogger.identity == 0 && !Logger.ArchestrALogger.Initialize() || (getMessage == null || !Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.logFlags[5], 0)))
          return;
        string str = getMessage();
        string message = string.Format((IFormatProvider) CultureInfo.InvariantCulture, "{0} method {1}. {2}", (object) prefix, (object) callerName, string.IsNullOrEmpty(str) ? (object) string.Empty : (object) str);
        Logger.NativeMethods.LogEntryExit(Logger.ArchestrALogger.identity, Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.includeCallerInfoLogFlag, 0) ? Logger.FormatMessageWithCallerInfo(message) : message);
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static void LogSQL(Func<string> getMessage)
      {
        if (Logger.ArchestrALogger.identity == 0 && !Logger.ArchestrALogger.Initialize() || (getMessage == null || !Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.logFlags[7], 0)))
          return;
        Logger.NativeMethods.LogSQL(Logger.ArchestrALogger.identity, Logger.NativeMethods.LogFlagLog(Logger.ArchestrALogger.identity, Logger.ArchestrALogger.includeCallerInfoLogFlag, 0) ? Logger.FormatMessageWithCallerInfo(getMessage()) : getMessage());
      }

      [SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Justification = "Method does not have to be used by all clients of the Logger.", MessageId = "ArchestrA.Diagnostics.Logger+NativeMethods.SetIdentityName(System.Int32,System.String)")]
      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static void LogSetIdentityName(string identityName)
      {
        if (Logger.ArchestrALogger.identity == 0 && !Logger.ArchestrALogger.Initialize())
          return;
        Logger.NativeMethods.SetIdentityName(Logger.ArchestrALogger.identity, identityName);
      }

      [SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", Justification = "Can't rename because of legacy reasons", MessageId = "Flag")]
      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static IntPtr RegisterCustomLogFlag(string flagName)
      {
        if (!Logger.ArchestrALogger.Initialize())
          return IntPtr.Zero;
        return Logger.NativeMethods.RegisterLogFlag(Logger.ArchestrALogger.identity, 11, flagName);
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static void ResetLoggerCheck()
      {
        Logger.ArchestrALogger.checkRegistry = true;
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      [SecurityPermission(SecurityAction.Demand, Unrestricted = true)]
      private static bool InitLoggerDll()
      {
        if (Logger.ArchestrALogger.isLoaded)
          return true;
        if (Logger.ArchestrALogger.checkRegistry)
        {
          IntPtr num = IntPtr.Zero;
          Logger.ArchestrALogger.checkRegistry = false;
          new RegistryPermission(RegistryPermissionAccess.Read, "HKEY_LOCAL_MACHINE\\Software\\ArchestrA\\Framework\\Logger").Assert();
          try
          {
            RegistryKey registryKey1 = Registry.LocalMachine.OpenSubKey("Software\\ArchestrA\\Framework\\Logger", false);
            if (registryKey1 != null)
            {
              string path1 = Convert.ToString(registryKey1.GetValue("InstallPath", (object) string.Empty), (IFormatProvider) CultureInfo.InvariantCulture);
              if (path1.Length > 0)
                num = Logger.NativeMethods.LoadLibraryW(Path.Combine(path1, "LoggerDll.dll"));
              registryKey1.Close();
            }
            else
            {
              bool flag = false;
              using (RegistryKey registryKey2 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32))
              {
                using (RegistryKey registryKey3 = registryKey2.OpenSubKey("SOFTWARE\\ArchestrA\\Framework\\Logger", false))
                  flag = registryKey3 != null && registryKey3.GetValue("InstallPath") != null && !string.IsNullOrEmpty(registryKey3.GetValue("InstallPath").ToString());
              }
              if (flag)
              {
                string[] files = Directory.GetFiles(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "LoggerDll.dll", SearchOption.AllDirectories);
                if (files.Length != 0)
                  num = Logger.NativeMethods.LoadLibraryW(files[0]);
              }
            }
          }
          finally
          {
            CodeAccessPermission.RevertAssert();
          }
          Logger.ArchestrALogger.isLoaded = num != IntPtr.Zero;
        }
        return Logger.ArchestrALogger.isLoaded;
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      [SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Justification = "Unused in the logic", MessageId = "ArchestrA.Diagnostics.Logger+NativeMethods.SetIdentityName(System.Int32,System.String)")]
      [SecurityPermission(SecurityAction.Demand, Unrestricted = true)]
      private static bool Initialize()
      {
        if (Logger.ArchestrALogger.domainUnloaded)
          return false;
        if (Logger.ArchestrALogger.identity != 0)
          return true;
        lock (Logger.ArchestrALogger.lockObj)
        {
          if (Logger.ArchestrALogger.identity == 0)
          {
            if (Logger.ArchestrALogger.InitLoggerDll())
            {
              int hIdentity = 0;
              int num = Logger.NativeMethods.RegisterLoggerClient(ref hIdentity);
              Logger.ArchestrALogger.identity = hIdentity;
              if (num != 0)
              {
                if (Logger.ArchestrALogger.identity != 0)
                {
                  new FileIOPermission(PermissionState.Unrestricted).Assert();
                  try
                  {
                    Logger.NativeMethods.SetIdentityName(Logger.ArchestrALogger.identity, Assembly.GetExecutingAssembly().GetName().Name);
                    Logger.ArchestrALogger.InitializeLogsFlags();
                  }
                  finally
                  {
                    CodeAccessPermission.RevertAssert();
                  }
                  AppDomain.CurrentDomain.DomainUnload += new EventHandler(Logger.ArchestrALogger.OnCurrentDomainUnload);
                  return true;
                }
              }
            }
          }
        }
        return false;
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      private static void InitializeLogsFlags()
      {
        if (Logger.NativeMethods.LogStart(Logger.ArchestrALogger.identity) <= 0)
          return;
        for (int nFlag = 0; nFlag < Logger.ArchestrALogger.logFlags.Length; ++nFlag)
          Logger.ArchestrALogger.logFlags[nFlag] = Logger.NativeMethods.GetLogFlag(Logger.ArchestrALogger.identity, nFlag);
        Logger.ArchestrALogger.includeCallerInfoLogFlag = Logger.RegisterCustomLogFlag("IncludeCallerInfo");
      }

      private static void OnCurrentDomainUnload(object sender, EventArgs e)
      {
        Logger.ArchestrALogger.UnInitialize();
        Logger.ArchestrALogger.domainUnloaded = true;
      }

      [SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Justification = "Method does not have to be used by all clients of the Logger.", MessageId = "ArchestrA.Diagnostics.Logger+NativeMethods.UnregisterLoggerClient(System.Int32)")]
      private static void UnInitialize()
      {
        if (Logger.ArchestrALogger.identity == 0)
          return;
        Logger.NativeMethods.UnregisterLoggerClient(Logger.ArchestrALogger.identity);
        Logger.ArchestrALogger.identity = 0;
      }

      private static bool IsLoggerInstalled()
      {
        bool flag;
        using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\ArchestrA\\Framework\\Logger", false))
          flag = registryKey != null && registryKey.GetValue("InstallPath") != null && !string.IsNullOrEmpty(registryKey.GetValue("InstallPath").ToString());
        if (flag)
          return true;
        using (RegistryKey registryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\ArchestrA\\Framework\\Logger", false))
          return registryKey != null && registryKey.GetValue("InstallPath") != null && !string.IsNullOrEmpty(registryKey.GetValue("InstallPath").ToString());
      }

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      private static string FormatMessageWithException(Func<string> getMessage, Exception exception)
      {
        string str = getMessage != null ? getMessage() : string.Empty;
        if (!string.IsNullOrEmpty(str))
        {
          if (exception != null)
            str = string.Format((IFormatProvider) CultureInfo.InvariantCulture, "{0} \n{1}", (object) str, (object) exception);
        }
        else if (exception != null)
          str = exception.ToString();
        return str;
      }
    }

    private static class LogFlagCategory
    {
      [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Native code")]
      internal const int LOG_ERROR = 0;
      [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Native code")]
      internal const int LOG_WARNING = 1;
      [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Native code")]
      internal const int LOG_INFO = 2;
      [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Native code")]
      internal const int LOG_TRACE = 3;
      [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Native code")]
      internal const int LOG_STARTSTOP = 4;
      [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Native code")]
      internal const int LOG_ENTRYEXIT = 5;
      [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Native code")]
      internal const int LOG_THREADSTARTSTOP = 6;
      [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Native code")]
      internal const int LOG_SQL = 7;
      [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Native code")]
      internal const int LOG_CONNECTION = 8;
      [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Native code")]
      internal const int LOG_CTORDTOR = 9;
      [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Native code")]
      internal const int LOG_REFCOUNT = 10;
      [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Native code")]
      internal const int LOG_CUSTOM = 11;
    }

    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed. Suppression is OK here.")]
    private static class NativeMethods
    {
      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      [DllImport("kernel32")]
      internal static extern IntPtr LoadLibraryW([MarshalAs(UnmanagedType.LPWStr)] string lpMdoule);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "REGISTERLOGGERCLIENT", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern int RegisterLoggerClient(ref int hIdentity);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "UNREGISTERLOGGERCLIENT", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern int UnregisterLoggerClient(int hIdentity);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "SETIDENTITYNAME", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern int SetIdentityName(int hIdentity, string strIdentity);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGERROR", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern void LogError(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string message);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGWARNING", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern void LogWarning(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string message);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGINFO", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern void LogInfo(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string message);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is shared code, not all consumers use all methods.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGTRACE", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern void LogTrace(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string message);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is shared code, not all consumers use all methods.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGSTARTSTOP", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern void LogStartStop(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string message);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is shared code, not all consumers use all methods.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGENTRYEXIT", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern void LogEntryExit(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string message);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is shared code, not all consumers use all methods.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGTHREADSTARTSTOP", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern void LogThreadStartStop(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string message);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is shared code, not all consumers use all methods.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGSQL", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern void LogSQL(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string message);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is shared code, not all consumers use all methods.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGCONNECTION", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern void LogConnection(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string message);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is shared code, not all consumers use all methods.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGCTORDTOR", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern void LogCtorDtor(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string message);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is shared code, not all consumers use all methods.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGREFCOUNT", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern void LogRefCount(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string message);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is shared code, not all consumers use all methods.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "REGISTERLOGFLAG", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern IntPtr RegisterLogFlag(
        int hIdentity,
        int nCustomFlag,
        [MarshalAs(UnmanagedType.LPWStr)] string strFlag);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is shared code, not all consumers use all methods.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGCUSTOM2", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern void LogCustom(int hIdentity, IntPtr customFlag, [MarshalAs(UnmanagedType.LPWStr)] string message);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "GETLOGFLAG", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern IntPtr GetLogFlag(int hIdentity, int nFlag);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGFLAGLOG", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      [return: MarshalAs(UnmanagedType.I1)]
      internal static extern bool LogFlagLog(int hIdentity, IntPtr logflag, int nFlag);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "LOGSTART", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern int LogStart(int hIdentity);

      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This is shared code, not all consumers use all methods.")]
      [DllImport("LoggerDLL.dll", EntryPoint = "GETLOGGERSTATS", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
      internal static extern int GetLoggerStats(
        [MarshalAs(UnmanagedType.LPWStr)] string hostName,
        ref int errorCount,
        ref long ftLastError,
        ref int warningCount,
        ref long ftLastWarning);
    }

    private static class WindowsLogger
    {
      [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Unused for now", MessageId = "entryType")]
      [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", Justification = "Unused for now", MessageId = "message")]
      [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Method does not have to be used by all clients of the Logger.")]
      internal static void WriteEntry(string message, EventLogEntryType entryType)
      {
      }
    }
  }
}
